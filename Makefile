.PHONY: clean-all clean-kaapana clean-vm fmt update-submodules build/vhdx-zips build/vmdk-zips build/qemu-zips build/zips build/vms-all build/vms-vhdx build/vms-vmdk


export CONTAINER_REGISTRY_USERNAME=changeme
export CONTAINER_REGISTRY_PASSWORD=changeme
export VERSION_IMAGE_COUNT=107
PLATFORM_VERSION := $(shell git describe)
# export LATEST=--latest
ifeq ($(LATEST),--latest)
export PLATFORM_VERSION=0.0.0-latest
endif

ROOT_DIR:=$(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))


update-submodules kaapana:
	echo "--> kaapana submodule not found! Pulling kaapana ...";
	git submodule update --init --recursive
	git submodule foreach --recursive 'git fetch --tags'

build/kaapana build/kaapana/kaapana-minimal-chart/kaapana-minimal-chart-${PLATFORM_VERSION}.tgz: kaapana
	echo "--> kaapana-minimal-chart-${PLATFORM_VERSION}.tgz not found! Building Kaapana with offline mode ...";
	./kaapana/build-scripts/start_build.py -c ${ROOT_DIR}/kaapana_minimal_default_build_config.yaml -kd ${ROOT_DIR}/kaapana -es ${ROOT_DIR} -bd ${ROOT_DIR}/build/kaapana -ll INFO ${LATEST}

build/kaapana-offline build/kaapana/kaapana-minimal-chart/kaapana-minimal-chart-${PLATFORM_VERSION}-images.tar: kaapana
	echo "--> kaapana-minimal-chart-${PLATFORM_VERSION}.tgz not found! Building Kaapana with offline mode ...";
	./kaapana/build-scripts/start_build.py -c ${ROOT_DIR}/kaapana_minimal_default_build_config.yaml -kd ${ROOT_DIR}/kaapana -es ${ROOT_DIR} -bd ${ROOT_DIR}/build/kaapana -ll INFO -oi ${LATEST}

build/kaapana-force: kaapana
	rm -rf ${ROOT_DIR}/build/kaapana
	echo "--> Kaapana --latest build ...";
	./kaapana/build-scripts/start_build.py -c ${ROOT_DIR}/kaapana_minimal_default_build_config.yaml -kd ${ROOT_DIR}/kaapana -es ${ROOT_DIR} -bd ${ROOT_DIR}/build/kaapana -ll INFO ${LATEST}

clean-all:
	rm -rf ./build

clean-kaapana:
	rm -rf ./build/kaapana
